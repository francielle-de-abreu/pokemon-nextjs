import axios from 'axios';
import { GetStaticPaths, GetStaticProps } from 'next';
import { PokemonProps } from '../../components/Pokemon/Pokemon.type';
import { Pokemon } from '../../components';

function PokeDetails({ pokemon }: PokemonProps) {
  return <div>{pokemon && <Pokemon pokemon={pokemon} />}</div>;
}

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await axios.get('https://pokeapi.co/api/v2/pokemon/');
  const pokemons = await response.data.results;
  const paths = pokemons.map((pokemon) => ({
    params: { name: String(pokemon.name) },
  }));

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps<PokemonProps> = async ({ params }) => {
  const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${String(params.name)}`);
  const pokemon = await response.data;

  return {
    props: { pokemon },
  };
};

export default PokeDetails;
