import Head from 'next/head';
import { useState, useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { CharacterList, Search, Button, Favorites } from '../components';
import { addFavoriteCharacter, addFavoriteList, getPokemons } from '../redux/actions/add-favorite-pokemon';
import { RootState } from '../redux/reducers/rootReducer';

function Pokemon() {
  const [inputValue, setInputValue] = useState('');
  const [pokemon, setPokemon] = useState([]);
  const favorites = useSelector((state: RootState) => state.charactersList.characters);
  const listFavorites = useSelector((state: RootState) => state.charactersList.listCharacters);
  const pokemons = useSelector((state: RootState) => state.pokemonReducer.pokemon.results);
  const loading = useSelector((state: RootState) => state.pokemonReducer.loading);
  const error = useSelector((state: RootState) => state.pokemonReducer.error);

  const dispatch = useDispatch();

  useEffect(() => {
    const localData = JSON.parse(window.localStorage.getItem('character'));
    dispatch(addFavoriteList(localData));
    dispatch(getPokemons());
  }, [dispatch]);

  const clear = useCallback(() => {
    setInputValue('');
    setPokemon([]);
  }, []);

  const handleFilter = (characters, value) => {
    const filterPokemon = characters.filter((character) => character.name == value.toLowerCase());
    setPokemon(filterPokemon);
  };

  const handleCharacter = (selectCara) => {
    const isFavorite = favorites.filter((item) => item != null).some((character) => character.name === selectCara.name);
    if (!isFavorite) {
      dispatch(addFavoriteCharacter(selectCara));
    }
  };

  return (
    <>
      <Head>
        <title>Pokemon-Nextjs</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="theme-color" content="Pokemon" />
        <meta name="description" content="Pokemon" />
        <meta name="author" content="Francielle Abreu" />
        <meta name="keywords" content="pokemon, web, nextjs" />
      </Head>
      <Search
        inputValue={inputValue}
        onClick={() => handleFilter(pokemons, inputValue)}
        description="Search"
        onChange={(e) => {
          setInputValue(e.target.value);
        }}
      />
      {loading && (
        <div className="display">
          <h3>Loading ...</h3>
          <div className="loading" />
        </div>
      )}
      {error && !loading && <p>{error}</p>}
      {pokemon.length === 0 && pokemons && pokemons.length > 0 ? (
        <CharacterList list={pokemons} handleCharacter={handleCharacter} />
      ) : (
        <>
          <CharacterList list={pokemon} />
          <div className="display">
            <Button onClick={clear} description="Back" />
          </div>
        </>
      )}
      {favorites.length === 0 && listFavorites.length > 0 ? (
        <Favorites list={listFavorites} />
      ) : (
        favorites.length > 0 && <Favorites list={listFavorites} />
      )}
    </>
  );
}

export default Pokemon;
