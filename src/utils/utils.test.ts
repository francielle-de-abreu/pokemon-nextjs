import { HandleIdImage } from './utils';

describe('Unit tests utils', () => {
  it('should return the id of this url', () => {
    const result = HandleIdImage('https://pokeapi.co/api/v2/pokemon/1/');
    const expected = '1';
    expect(expected).toEqual(result);
  });
});
