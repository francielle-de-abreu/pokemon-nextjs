export const HandleIdImage = (url) => {
  const brokenUrl = url.split("/");
  const id = brokenUrl[brokenUrl.length - 2];
  return id;
};
