import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../api';

function* fetchCharacters(action) {
  try {
    const pokemon = yield call(api.get);
    yield put({ type: 'POKEMON_FETCH_SUCCESS', pokemon: pokemon });
  } catch (e) {
    yield put({ type: 'POKEMON_FETCH_FAILED', message: e.message });
  }
}

function* characterSaga() {
  yield takeLatest('GET_CHARACTERS_REQUESTED', fetchCharacters);
}

export default characterSaga;
