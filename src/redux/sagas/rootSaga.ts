import { all } from 'redux-saga/effects';
import characterSaga from './pokemonSaga';

const sagas = [characterSaga()];

export default function* rootSaga() {
  yield all(sagas);
}
