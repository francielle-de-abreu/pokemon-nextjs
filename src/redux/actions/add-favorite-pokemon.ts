import { ADD_FAVORITE_POKEMON, ADD_FAVORITE_LIST, GET_CHARACTERS_REQUESTED } from './type';

export const addFavoriteCharacter = (character) => ({
  type: ADD_FAVORITE_POKEMON,
  character,
});

export const addFavoriteList = (charactersList) => ({
  type: ADD_FAVORITE_LIST,
  charactersList,
});

export const getPokemons = () => ({
  type: GET_CHARACTERS_REQUESTED,
});
