import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import rootReducer from '../reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();
const middlewares = applyMiddleware(sagaMiddleware);
const composition = composeWithDevTools(middlewares);

// const store = createStore(rootReducer, composition);

const store = compose(middlewares, composition)(createStore)(rootReducer);
sagaMiddleware.run(rootSaga);

export default store;
