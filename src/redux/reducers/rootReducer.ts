import { combineReducers } from 'redux';
import charactersList from './characters-reducer';
import pokemonReducer from './pokemonRequest';

const rootReducer = combineReducers({ charactersList, pokemonReducer });

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
