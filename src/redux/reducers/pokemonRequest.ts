import { POKEMON_FETCH_SUCCESS, POKEMON_FETCH_FAILED, GET_CHARACTERS_REQUESTED } from '../actions/type';

const defaulState = {
  pokemon: [],
  loading: false,
  error: null,
};

function pokemonReducer(state = defaulState, action) {
  switch (action.type) {
    case GET_CHARACTERS_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case POKEMON_FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        pokemon: action.pokemon,
      };
    case POKEMON_FETCH_FAILED:
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    default:
      return state;
  }
}

export default pokemonReducer;
