import { ADD_FAVORITE_POKEMON, ADD_FAVORITE_LIST } from '../actions/type';

const defaulState = {
  characters: [],
  listCharacters: [],
};

function charactersList(state = defaulState, action) {
  if (action.charactersList !== null) {
    switch (action.type) {
      case ADD_FAVORITE_POKEMON:
        state.listCharacters.push(action.character);
        window.localStorage.setItem('character', JSON.stringify(state.listCharacters));
        return {
          ...state,
          characters: [...state.listCharacters, action.character],
        };
      case ADD_FAVORITE_LIST:
        return {
          ...state,
          listCharacters: action.charactersList,
        };
      default:
        return state;
    }
  } else {
    return defaulState;
  }
}

export default charactersList;
