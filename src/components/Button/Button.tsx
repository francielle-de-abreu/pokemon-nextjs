import * as Style from './Button.style';
import { ButtonProps } from './Button.type';

const Button = ({ onClick, description }: ButtonProps) => (
  <Style.Main data-testid="button" onClick={onClick} type="submit">
    {description}
  </Style.Main>
);

export default Button;
