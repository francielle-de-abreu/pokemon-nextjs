import Button from './Button';
import { ButtonProps } from './Button.type';

export default {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    onClick: {
      action: 'onClick',
    },
    description: {
      control: 'text',
      defaultValue: 'Search',
    },
  },
};

export const Default = ({ onClick, description }: ButtonProps) => (
  <Button onClick={onClick} description={description} />
);
