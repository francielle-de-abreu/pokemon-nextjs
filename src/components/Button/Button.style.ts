import styled from 'styled-components';

export const Main = styled.button`
  background-color: #1a1a1a;
  border: 1px solid transparent;
  color: white;
  font-size: 1.1rem;
  font-size: 1.5rem;
  font-weight: 700;
  height: 60px;
  letter-spacing: 3px;
  text-align: center;
  width: 150px;

  &:hover {
    background-color: #1890ff;
    border-color: #1890ff;
    color: white;
    cursor: pointer;
  }
`;
