export type ButtonProps = {
  onClick?: () => void;
  description: string;
};
