import styled from 'styled-components';

export const Main = styled.div`
  margin: auto;
  padding: 3%;
  width: 67%;
`;

export const Container = styled.div`
  align-items: center;
  background-image: linear-gradient(to right, #cf0063, #a71065, #8b105d, #3c0e49);
  border: 1px solid #6b6b6b;
  color: #fff;
  display: flex;
  flex-wrap: wrap;
  font-size: 25px;
  justify-content: center;
  margin-bottom: 0;
  padding: 16px;
`;
export const Item = styled.div`
  background-color: #eceff1;
  border: 1px solid #6b6b6b;
  border-top: none;
  color: #6b6b6b;
  font-size: 20px;
  font-weight: 700;
  padding: 1%;
`;
