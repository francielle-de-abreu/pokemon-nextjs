import { FavoritesProps } from './Favorites.type';
import * as Style from './Favorites.style';

const Favorites = ({ list }: FavoritesProps) => {
  return (
    <Style.Main data-testid="favorites">
      <Style.Container>Favorites</Style.Container>
      {list.length > 0 &&
        list.filter((item) => item != null).map(({ name }) => <Style.Item key={name}>{name}</Style.Item>)}
    </Style.Main>
  );
};

export default Favorites;
