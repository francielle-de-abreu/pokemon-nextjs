import Favorites from './Favorites';
import { FavoritesProps } from './Favorites.type';

const items = [
  { name: 'bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
  { name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2/' },
];

export default {
  title: 'Components/Favorites',
  component: Favorites,
  argTypes: {
    list: {
      control: 'object',
      defaultValue: items,
    },
  },
};

export const Default = ({ list }: FavoritesProps) => <Favorites list={list} />;
