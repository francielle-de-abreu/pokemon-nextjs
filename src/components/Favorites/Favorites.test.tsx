import { render, screen } from '@testing-library/react';
import Favorites from './Favorites';

const list = [
  { name: 'bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
  { name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2/' },
];

describe('Favorites Component', () => {
  it('should renders correctly', () => {
    render(<Favorites list={list} />);
    expect(screen.getByTestId('favorites')).toBeInTheDocument();
  });
});
