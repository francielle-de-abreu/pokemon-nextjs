import styled from 'styled-components';

export const Main = styled.input`
  background-color: #eceff1;
  border: none;
  border-radius: 0.4rem;
  border-right: none;
  font-size: 20px;
  margin: auto;
  margin: 0;
  padding: 1%;
  width: 54%;

  @media (max-width: 700px) {
    font-size: 17px;
    margin: 20px;
    padding: 10px;
    width: 100%;
  }
  @media (max-width: 400px) {
    margin-right: 60px;
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  padding: 40px;
  width: 100%;

  @media (max-width: 630px) {
    display: block;
    text-align: center;
  }
`;
