import * as Style from './Search.style';
import { Button } from '../../components';
import { InputProps } from './Search.type';

const Search = ({ onChange, inputValue, description, onClick }: InputProps) => {
  return (
    <Style.Container data-testid="search">
      <Style.Main onChange={onChange} value={inputValue} placeholder="What Pokémon are you looking for?" />
      <Button data-testid="button" onClick={onClick} description={description} />
    </Style.Container>
  );
};
export default Search;
