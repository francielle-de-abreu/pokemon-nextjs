import Search from './Search';
import { InputProps } from './Search.type';

export default {
  title: 'Components/Search',
  component: Search,
  argTypes: {
    onClick: {
      action: 'onClick',
    },
    onChange: {
      action: 'onChange',
    },
    description: {
      control: 'text',
      defaultValue: 'Search',
    },
    inputValue: {
      control: 'text',
      defaultValue: 'bulbasaur',
    },
  },
};

export const Default = ({ onChange, inputValue, description, onClick }: InputProps) => (
  <Search onChange={onChange} inputValue={inputValue} description={description} onClick={onClick} />
);
