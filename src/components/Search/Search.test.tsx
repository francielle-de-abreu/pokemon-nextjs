import React from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Search from './Search';
import Button from '../Button';

describe('DefaultSearch component', () => {
  it('should render correctly', () => {
    render(<Search description="Search" />);
    expect(screen.getByTestId('search')).toBeInTheDocument();
  });

  it('should render button correctly', () => {
    render(<Button description="button" />);
    expect(screen.getByTestId('button')).toBeInTheDocument();
  });

  it('should call onButtonClick when button was clicked', () => {
    const onButtonClick = jest.fn();
    render(<Search onClick={onButtonClick} description="button" />);
    userEvent.click(screen.getByTestId('button'));
    expect(onButtonClick).toBeCalledTimes(1);
  });
});
