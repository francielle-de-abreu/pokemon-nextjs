export type InputProps = {
  onChange?: (event) => void;
  inputValue?: string;
  description: string;
  onClick?: () => void;
};
