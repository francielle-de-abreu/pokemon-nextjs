type Types = {
  type: { name: string; url: string };
};

type Ability = {
  ability: { name: string; url: string };
};

export type Pokemon = {
  url: string;
  name: string;
  sprites?: { front_default: string };
  base_experience?: number;
  types?: Array<Types>;
  abilities?: Array<Ability>;
};

export type PokemonProps = {
  pokemon: Pokemon;
};

export type CharactersProps = {
  characters?: [];
};
