import * as Style from './Pokemon.style';
import { PokemonProps } from './Pokemon.type';

const Pokemon = ({ pokemon }: PokemonProps) => {
  return (
    <div>
      {pokemon && (
        <Style.Main data-testid="pokemon">
          <Style.Title>{pokemon.name.toUpperCase()}</Style.Title>
          <Style.Container>
            <Style.Image src={pokemon.sprites.front_default} />
            <Style.Text>Base experience:{pokemon.base_experience}</Style.Text>
          </Style.Container>
          <Style.Details>
            <Style.Info>
              {pokemon.types.map((item) => (
                <Style.Text key={item.type.name}>{item.type.name}</Style.Text>
              ))}
            </Style.Info>
            <Style.Info>
              {pokemon.abilities.map((item) => (
                <Style.Text key={item.ability.name}>{item.ability.name}</Style.Text>
              ))}
            </Style.Info>
          </Style.Details>
        </Style.Main>
      )}
    </div>
  );
};
export default Pokemon;
