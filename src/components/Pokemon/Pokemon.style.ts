import styled from 'styled-components';

export const Main = styled.div`
  display: flex;
  flex-wrap: wrap;
  font-size: 20px;
  justify-content: center;
`;

export const Details = styled.div`
  @media (max-width: 500px) {
    display: flex;
    font-size: 20px;
    justify-content: center;
  }
`;
export const Title = styled.h1`
  background-image: linear-gradient(to right, #6b6b6b, #1b1b1b);
  color: #ffff;
  margin-bottom: 10%;
  padding: 16px;
  text-align: center;
  width: 100%;
`;

export const Container = styled.div`
  border: 0.5px solid #6b6b6b;
  color: #6b6b6b;
  padding: 32px;
  text-align: center;

  @media (max-width: 500px) {
    width: 79%;
  }

  @media (max-width: 400px) {
    padding: 32px;
    width: 100%;
  }
`;

export const Info = styled.div`
  background-image: linear-gradient(to right, #cf0063, #a71065, #8b105d, #3c0e49);
  border: 0.5px solid #6b6b6b;
  border-left: none;
  color: #ffff;
  padding: 45px;
  text-align: center;

  @media (max-width: 500px) {
    padding: 43px;
    width: 100%;
  }
`;

export const Image = styled.img`
  height: 120px;
  width: 120px;
`;

export const Text = styled.p`
  margin: 0;
`;
