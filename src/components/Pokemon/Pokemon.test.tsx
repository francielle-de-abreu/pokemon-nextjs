import { render, screen } from '@testing-library/react';
import Pokemon from './Pokemon';

const pokemon = {
  name: 'bulbasaur',
  sprites: { front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png' },
  url: 'https://pokeapi.co/api/v2/pokemon/1/',
  base_experience: 15,
  abilities: [{ ability: { name: 'overgrow', url: 'https://pokeapi.co/api/v2/ability/65/' } }],
  types: [{ type: { name: 'grass', url: 'https://pokeapi.co/api/v2/type/12/' } }],
};

describe('Pokemon Component', () => {
  it('should renders correctly', () => {
    render(<Pokemon pokemon={pokemon} />);
    expect(screen.getByTestId('pokemon')).toBeInTheDocument();
  });
  it('should that it renders correctly in the snapshot ', () => {
    render(<Pokemon pokemon={pokemon} />);
    expect(screen.getByTestId('pokemon')).toMatchSnapshot();
  });
});
