import Pokemon from './Pokemon';
import { PokemonProps } from './Pokemon.type';

const item = {
  name: 'bulbasaur',
  sprites: { front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png' },
  url: 'https://pokeapi.co/api/v2/pokemon/1/',
  base_experience: 15,
  abilities: [{ ability: { name: 'overgrow', url: 'https://pokeapi.co/api/v2/ability/65/' } }],
  types: [{ type: { name: 'grass', url: 'https://pokeapi.co/api/v2/type/12/' } }],
};

export default {
  title: 'Components/Pokemon',
  component: Pokemon,
  argTypes: {
    pokemon: {
      control: 'object',
      defaultValue: item,
    },
  },
};

export const Default = ({ pokemon }: PokemonProps) => <Pokemon pokemon={pokemon} />;
