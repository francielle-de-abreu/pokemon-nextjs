import Link from 'next/link';
import * as Style from './Characters.style';
import { ListProps } from './Characters.type';
import { HandleIdImage } from '../../utils/utils';

const CharacterList = ({ list, handleCharacter }: ListProps) => {
  return (
    <Style.Main data-testid="characters">
      {list.map(({ name, url }) => (
        <Style.Container key={name}>
          <Link href="/poke-detail/[name]" as={`/poke-detail/${name}`}>
            <Style.Anchor data-testid={`characters-link-${name}`}>{name}</Style.Anchor>
          </Link>
          <Style.Image
            data-testid={`characters-image-${name}`}
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${HandleIdImage(url)}.png`}
            alt={` Pokemon: ${name}`}
          />
          <Style.Button data-testid={`characters-button-${name}`} onClick={() => handleCharacter({ name })}>
            Favorite
          </Style.Button>
        </Style.Container>
      ))}
    </Style.Main>
  );
};
export default CharacterList;
