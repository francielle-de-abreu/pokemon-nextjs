import styled from 'styled-components';

export const Main = styled.div`
  display: flex;
  flex-wrap: wrap;
  font-weight: 700;
  justify-content: center;
  margin: auto;
  width: 70%;
`;

export const Container = styled.div`
  align-items: center;
  background-image: linear-gradient(to right, #cf0063, #a71065, #8b105d, #3c0e49);
  border: 1px solid #6b6b6b;
  border-radius: 5px;
  display: flex;
  flex-wrap: wrap;
  height: 170px;
  justify-content: center;
  margin: 16px;
  padding: 16px;
  width: 248px;
`;

export const Anchor = styled.a`
  color: #ffff;
  cursor: pointer;
  font-size: 20px;
  &:hover {
    color: #03a9f4;
  }
`;

export const Image = styled.img`
  height: 96px;
  width: 96px;
`;

export const Button = styled.button`
  background-color: #1a1a1a;
  color: #ffff;
  cursor: pointer;
  font-size: 17px;
  font-weight: 700;
  padding: 2%;
  width: 248px;

  &:hover {
    background-color: #1890ff;
    border-color: #1890ff;
    color: white;
    cursor: pointer;
  }
`;
