import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import CharacterList from './Characters';

const list = [
  { name: 'bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
  { name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2/' },
];

describe('CharacterList Component', () => {
  it('should renders correctly', () => {
    render(<CharacterList list={list} />);
    expect(screen.getByTestId('characters')).toBeInTheDocument();
    expect(screen.getByTestId('characters')).toHaveTextContent('bulbasaur');
    expect(screen.getByTestId('characters')).toHaveTextContent('ivysaur');
  });

  it('should renders image correctly', () => {
    render(<CharacterList list={list} />);
    const element = screen.getByTestId('characters-image-bulbasaur');
    expect(element).toBeInTheDocument();
  });

  it('should renders link correctly', () => {
    render(<CharacterList list={list} />);
    const element = screen.getByTestId('characters-link-bulbasaur');
    expect(`${window.location.href}poke-detail/bulbasaur`).toBe('http://localhost/poke-detail/bulbasaur');
    expect(element).toHaveTextContent('bulbasaur');
  });

  it('should call onButtonClick when button was clicked', () => {
    const onButtonClick = jest.fn();
    render(<CharacterList list={list} handleCharacter={onButtonClick} />);
    const element = screen.getByTestId('characters-button-bulbasaur');
    userEvent.click(element);
    expect(onButtonClick).toBeCalledTimes(1);
  });
});
