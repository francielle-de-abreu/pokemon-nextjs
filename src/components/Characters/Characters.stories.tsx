import CharacterList from './Characters';
import { ListProps } from './Characters.type';

const items = [
  { name: 'bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
  { name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2/' },
];

export default {
  title: 'Components/CharacterList',
  component: CharacterList,
  argTypes: {
    list: {
      control: 'object',
      defaultValue: items,
    },
    handleCharacter: {
      action: 'onClick',
    },
  },
};

export const Default = ({ list, handleCharacter }: ListProps) => (
  <CharacterList list={list} handleCharacter={handleCharacter} />
);
