export type Character = {
  name: string;
  url: string;
};

export type ListProps = {
  list?: Character[];
  handleCharacter?: (name) => void;
};
