import config from '../config';

function get() {
  return fetch(config.baseUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((reponse) => reponse.json())
    .catch((error) => {
      throw error;
    });
}

const api = {
  get,
};

export default api;
